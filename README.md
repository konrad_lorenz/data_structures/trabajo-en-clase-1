# Trabajo  en clase 1


## Paso 1

Construya a lapiz y papel un arbol binario a partir de la siguiente lista de numeros:

```

valores = [17, 9, 26, 4, 13, 22, 30, 2, 6, 11, 15, 20, 24, 28, 32, 1, 3, 5, 7, 10, 12, 14, 16, 19, 21, 23, 25, 27, 29, 31, 33, 34]

```

## Paso 2

A partir del siguiente Pseudocodigo implementar en google colab el algoritmo para busqueda e insercion en arboles binarios, documentar la complejidad asintotica (Big-O).


```
Clase Nodo:
    Entero valor
    Nodo izquierda
    Nodo derecha

Clase ArbolBinarioBusqueda:
    Nodo raiz

    Método Inicializar():
        raiz = Nulo

    Método Insertar(valor):
        raiz = InsertarRecursivamente(raiz, valor)

    Método InsertarRecursivamente(nodo, valor):
        Si nodo es Nulo:
            Crear un nuevo nodo con valor y devolverlo
        Si valor < nodo.valor:
            nodo.izquierda = InsertarRecursivamente(nodo.izquierda, valor)
        Sino:
            nodo.derecha = InsertarRecursivamente(nodo.derecha, valor)
        Devolver nodo

    Método Buscar(valor):
        Devolver BuscarRecursivamente(raiz, valor)

    Método BuscarRecursivamente(nodo, valor):
        Si nodo es Nulo:
            Devolver Falso
        Si valor es igual a nodo.valor:
            Devolver Verdadero
        Sino Si valor < nodo.valor:
            Devolver BuscarRecursivamente(nodo.izquierda, valor)
        Sino:
            Devolver BuscarRecursivamente(nodo.derecha, valor)

    Método ImprimirArbol():
        ImprimirArbolRecursivamente(raiz, "", Verdadero)

    Método ImprimirArbolRecursivamente(nodo, prefijo, es_ultimo):
        Si nodo no es Nulo:
            Imprimir(prefijo + "└── " si es_ultimo, prefijo + "├── ")
            Imprimir nodo.valor
            ImprimirArbolRecursivamente(nodo.izquierda, prefijo + ("    " si es_ultimo, "│   "), Falso)
            ImprimirArbolRecursivamente(nodo.derecha, prefijo + ("    " si es_ultimo, "│   "), Verdadero)

arbol = Nueva Instancia de ArbolBinarioBusqueda()
valores = [17, 9, 26, 4, 13, 22, 30, 2, 6, 11, 15, 20, 24, 28, 32, 1, 3, 5, 7, 10, 12, 14, 16, 19, 21, 23, 25, 27, 29, 31, 33, 34]


Para cada valor en valores:
    arbol.Insertar(valor)

arbol.ImprimirArbol()

valor_a_buscar = 7

Si arbol.Buscar(valor_a_buscar):
    Imprimir "El valor " + valor_a_buscar + " fue encontrado en el árbol."
Sino:
    Imprimir "El valor " + valor_a_buscar + " no fue encontrado en el árbol."

```


## Paso 3

Validar el resultado del punto 1 con el resultado del paso 2 y documentar a nivel de reporte los resultados.